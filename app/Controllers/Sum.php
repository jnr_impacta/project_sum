<?php
/**
 * Miscellaneous (helper) Functions Doc
 *
 * PHP version 5
 *
 * @category File
 * @package  Misc_Functions
 * @author   Ademilson Junior <ademilson.junior@aluno.faculdadeimpacta.com.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://grupoa-devsecops.herokuapp.com/
 */

namespace Controllers;

/**
 * Sum class that will add N arguments and return sum
 *
 * @category Class
 * @package  Sum_Custom
 * @author   Ademilson Junior <ademilson.junior@aluno.faculdadeimpacta.com.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     https://grupoa-devsecops.herokuapp.com/
 */
class Sum
{
    /**
 * This function will add N numbers and return the result.
 *
 * Given n arguments, this function will return the sum.
 *
 * @return int
 */
    public function getSum() 
    {
        $args = func_get_args();

        $sum = 0;
        if (empty($args)) { 
            return $sum;
        }

        foreach ($args as $arg) {
            $sum += $arg;
        }

        return $sum;
    }
}
?>
