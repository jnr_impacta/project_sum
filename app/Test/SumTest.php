<?php

namespace app\Test;
require __DIR__ . "/../Controllers/Sum.php";

class SumTest extends \PHPUnit_Framework_TestCase
{
	public function testPositiveNumbers()
	{
		$s = new \Controllers\Sum();
		$this->assertEquals(6, $s->getSum(5, 1));
		$this->assertEquals(102, $s->getSum(100, 2));
	}
	
	public function testMultipleNumbers()
	{
		$s = new \Controllers\Sum();
		$this->assertEquals(0, $s->getSum());
		$this->assertEquals(6, $s->getSum(5, 1));
		$this->assertEquals(9, $s->getSum(5, 1, 3));
		$this->assertEquals(120, $s->getSum(100, 2, 8, 10));
	}
	
	public function testNegativeNumbers()
	{
		$s = new \Controllers\Sum();
		$this->assertEquals(4, $s->getSum(5, -1));
		$this->assertEquals(7, $s->getSum(5, -1, 3));
	}
}